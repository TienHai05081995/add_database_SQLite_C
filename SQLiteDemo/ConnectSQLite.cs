﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace SQLiteDemo
{
    class ConnectSQLite
    {
        public SQLiteConnection m_connection;
        string filePath = "../../SAB.db";
        public ConnectSQLite()
        {
            if (!File.Exists(filePath))
            {
                SQLiteConnection.CreateFile(filePath);
            }
            m_connection = new SQLiteConnection("Data source=" + filePath);

        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public void OpenConnect()
        {

            if (m_connection.State != System.Data.ConnectionState.Open)
            {
                m_connection.Open();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public void CloseConnect()
        {
            if (m_connection.State != System.Data.ConnectionState.Closed)
            {
                m_connection.Close();
            }
        }
    }
}
