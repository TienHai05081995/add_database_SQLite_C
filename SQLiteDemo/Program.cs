﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace SQLiteDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ConnectSQLite databaseObject = new ConnectSQLite();
            string querry = "INSERT INTO Login ([ID], [Name], [SDT], [Age]) VALUES (@ID, @Name, @SDT, @Age)";
            SQLiteCommand conn = new SQLiteCommand(querry, databaseObject.m_connection);
            databaseObject.OpenConnect();
            conn.Parameters.AddWithValue("@ID", "1001");
            conn.Parameters.AddWithValue("@Name", "Linh");
            conn.Parameters.AddWithValue("@SDT", "12345678");
            conn.Parameters.AddWithValue("@Age", "27");
            var result = conn.ExecuteNonQuery();
            databaseObject.CloseConnect();


            Console.ReadKey();

        }
    }
}
